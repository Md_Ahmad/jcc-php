<?php 
require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

$animals = new Animal("Shaun");
echo "Name : " . $animals->name;
echo "<br>";
echo "Legs : " . $animals->legs;
echo "<br>";
echo "cold_blooded : " . $animals->cold_blooded;
echo "<br><br>";

$Frog = new frog("Buduk");
echo "Name : " . $Frog->name;
echo "<br>";
echo "Legs : " . $Frog->legs;
echo "<br>";
echo "cold_blooded : " . $Frog->cold_blooded;
echo "<br>";
echo "Jump : " . $Frog->jump();
echo "<br><br>";


$Ape = new Ape("Kera Sakti");
echo "Name : " . $Ape->name;
echo "<br>";
echo "Legs : " . $Ape->legs;
echo "<br>";
echo "cold_blooded : " . $Ape->cold_blooded;
echo "<br>";
echo "Yell : " . $Ape->yell();
echo "<br>";
?>